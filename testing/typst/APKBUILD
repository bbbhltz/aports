# Maintainer: psykose <alice@ayaya.dev>
pkgname=typst
pkgver=0.2.0
pkgrel=1
pkgdesc="New markup-based typesetting system that is powerful and easy to learn"
url="https://github.com/typst/typst"
# s390x: 90% of tests fail due to endianness
arch="all !s390x"
license="Apache-2.0"
makedepends="
	cargo
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/typst/typst/archive/refs/tags/v$pkgver.tar.gz"
options="net"

export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

case "$CARCH" in
riscv64)
	options="$options textrels"
	;;
esac

prepare() {
	default_prepare

	git init .
	git config user.name x
	git config user.email x@example.com
	git commit -m init --allow-empty
	git tag -m "$pkgver" -a "$pkgver"

	cargo fetch --target="$CTARGET" --locked
}

build() {
	# XXX: it vendors 5MB of fonts, but the font detection is kinda
	# broken and it won't find math symbols from system fonts correctly
	# (when you have a lot of fonts it seems?)
	# so, just keep them vendored-in for now. when fixed, add
	# --no-default-features
	GEN_ARTIFACTS="./gen" \
		cargo build --release --frozen -p typst-cli
}

check() {
	cargo test --frozen --all
}

package() {
	install -Dm755 target/release/typst \
		-t "$pkgdir"/usr/bin/

	install -Dm644 ./cli/gen/typst.bash "$pkgdir"/usr/share/bash-completion/completions/typst
	install -Dm644 ./cli/gen/typst.fish "$pkgdir"/usr/share/fish/completions/typst.fish
	install -Dm644 ./cli/gen/_typst "$pkgdir"/usr/share/zsh/site-functions/_typst
	install -Dm644 ./cli/gen/*.1 -t "$pkgdir"/usr/share/man/man1/
}

sha512sums="
200b86d2450532deec06338e250ccb76a424394fedf1747fe3b6337e6f9f187f100a60dfbb63232074c0da4282a1a464df9c824ba98e78773ce6645b2d3f54c9  typst-0.2.0.tar.gz
"
