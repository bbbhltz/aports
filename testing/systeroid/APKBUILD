# Contributor: Hoang Nguyen <folliekazetani@protonmail.com>
# Maintainer: omni <omni+alpine@hack.org>
pkgname=systeroid
pkgver=0.3.2
pkgrel=0
pkgdesc="A more powerful alternative to sysctl(8)"
# riscv64: rust broken
arch="all !riscv64"
url="https://systeroid.cli.rs/"
license="Apache-2.0"
makedepends="cargo libxcb-dev"
checkdepends="linux-lts-doc xclip"
subpackages="$pkgname-doc $pkgname-tui:_tui $pkgname-tui-doc:_tui_doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/orhun/systeroid/archive/refs/tags/v$pkgver.tar.gz"

# For armhf, no linux-lts-doc, nor linux-rpi-doc, also tests may stall
# For ppc64le CONFIG_BSD_PROCESS_ACCT is not set in lts.ppc64le.config
case $CARCH in
	armhf|ppc64le) options="!check" ;;
esac

export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	case "$CARCH" in
	s390x)
		# Fail to build nix crate
		cargo build --release --frozen -p systeroid
		cargo build --release --frozen --no-default-features -p systeroid-tui
		;;
	*)
		cargo build --release --frozen
		;;
	esac
}

check() {
	NO_COLOR=1 cargo test --frozen --no-default-features
}

package() {
	install -Dm0755 target/release/systeroid target/release/systeroid-tui \
		-t "$pkgdir"/usr/bin/
	install -Dm0644 man8/systeroid.8 -t "$pkgdir"/usr/share/man/man8/
}

_tui() {
	pkgdesc="$pkgname terminal user interface"
	amove usr/bin/systeroid-tui
}

_tui_doc() {
	pkgdesc="$pkgname terminal user interface (documentation)"

	cd "$builddir"
	install -Dm0644 man8/systeroid-tui.8 -t "$subpkgdir"/usr/share/man/man8/

	default_doc
	install_if="docs $pkgname-tui=$pkgver-r$pkgrel"
}

sha512sums="
31d4886e7384833b2ef4a0ccb3b8797068ee65360b5c4e2b049aee2055713c77887245ef35c3a9d49b7873dcc77f2877abe540b5eaaa385a9acfd02f0afce64f  systeroid-0.3.2.tar.gz
"
